import {Component, OnInit, Input} from '@angular/core';
import {FormControl} from '@angular/forms';
import {merge, Subject, combineLatest} from 'rxjs';
import {map, scan, tap, startWith} from 'rxjs/operators';

const ACTIVITIES = [
  {title: 'Вид деятельности 1', value: 0},
  {title: 'Вид деятельности 2', value: 1},
  {title: 'Вид деятельности 3', value: 2},
  {title: 'Вид деятельности 4', value: 3},
];

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
})
export class FormComponent implements OnInit {
  @Input() title: string;

  public activities = ACTIVITIES;
  public clickButton$: any;
  public orgnameControl: FormControl;
  public innControl: FormControl;
  public nameControl: FormControl;
  public posControl: FormControl;
  public numControl: FormControl;
  public emailControl: FormControl;
  public foaControl: FormControl;
  public fileControl: FormControl;
  public commentControl: FormControl;
  public values: any;

  constructor() {
    this.clickButton$ = new Subject();
  }

  ngOnInit(): void {
    this.orgnameControl = new FormControl();
    this.innControl = new FormControl();
    this.nameControl = new FormControl();
    this.posControl = new FormControl();
    this.numControl = new FormControl();
    this.emailControl = new FormControl();
    this.foaControl = new FormControl('0');
    this.fileControl = new FormControl();
    this.commentControl = new FormControl();
    this.mergeStreamsValues().subscribe((values) => {
      this.values = values;
    });
  }

  mergeStreamsValues() {
    const inputs$ = merge(
      this.orgnameControl.valueChanges.pipe(
        startWith(''),
        map((value) => ({
          orgname: {value, valid: value.length > 0},
        })),
      ),
      this.innControl.valueChanges.pipe(
        startWith(''),
        map((value) => ({inn: {value, valid: value.length > 0}})),
      ),
      this.nameControl.valueChanges.pipe(
        startWith(''),
        map((value) => ({name: {value, valid: value.length > 0}})),
      ),
      this.posControl.valueChanges.pipe(
        startWith(''),
        map((value) => ({pos: {value, valid: value.length > 0}})),
      ),
      this.numControl.valueChanges.pipe(
        startWith(''),
        map((value) => ({num: {value, valid: value.length > 0}})),
      ),
      this.emailControl.valueChanges.pipe(
        startWith(''),
        map((value) => ({email: {value, valid: value.length > 0}})),
      ),
      this.foaControl.valueChanges.pipe(
        startWith('0'),
        map((value) => ({foa: {value, valid: true}})),
      ),
      this.fileControl.valueChanges.pipe(
        startWith(''),
        map((value) => ({file: {value, valid: value.length > 0}})),
      ),
      this.commentControl.valueChanges.pipe(
        startWith(''),
        map((value) => ({comment: {value, valid: value.length > 0}})),
      ),
    ).pipe(
      scan((acc, item) => {
        return {...acc, ...item};
      }, {}),
    );
    return combineLatest([inputs$, this.clickButton$]).pipe(
      tap((value) => {
        console.log('value', value && value[0]);
        // API request
      }),
    );
  }

  sendForm() {
    this.clickButton$.next();
  }
}
