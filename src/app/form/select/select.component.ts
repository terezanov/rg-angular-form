import {Component, OnChanges, Input} from '@angular/core';

interface Item {
  title: string;
  value: number;
}

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
})
export class SelectComponent implements OnChanges {
  @Input() name: string;
  @Input() items: Array<Item>;
  @Input() control?: any;
  @Input() values?: any;

  public val: any;

  constructor() {}

  ngOnChanges(args): void {
    this.val = args.values.currentValue ? args.values.currentValue[0][this.name] : {valid: true};
    // console.log('select val', this.val);
  }
}
