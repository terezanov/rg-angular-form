import {Component, OnInit, Input, OnChanges} from '@angular/core';

const FILE_SIZE = 10000000;

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent implements OnInit, OnChanges {
  @Input() type?: string;
  @Input() label?: string;
  @Input() required?: boolean;
  @Input() name?: string;
  @Input() control?: any;
  @Input() values?: any;

  public val: any;
  public value: string;
  public message: string;
  public isTextarea: boolean;
  public isFile: boolean;

  constructor() {}

  handleInput(event: any) {
    const {files, value} = event.target;
    this.value = value || '';
    if (files != null) {
      this.value = value.slice(12);
      if (files.length > 0) {
        console.log('files', files);
        this.message =
          files[0].size > FILE_SIZE
            ? 'Размер файла не должен превышать 10 мб'
            : null;
      }
    }
  }

  ngOnInit(): void {
    this.isFile = this.type === 'file';
    this.isTextarea = this.type === 'textarea';
  }

  ngOnChanges(args): void {
    this.val = args.values.currentValue
      ? args.values.currentValue[0][this.name]
      : {valid: true};
    // console.log('val', this.val);
  }
}
